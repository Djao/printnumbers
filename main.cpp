/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <iostream> 
using namespace std;  

int main()
{
    for (int i = 1; i < 101; i++) {
        if(i%3 == 0) cout<<"Foo";
        if(i%5 == 0) cout<<"Baa";
        if(i%3 != 0 && i%5 != 0) cout<<i;
        cout<<endl;
    }
    return 0;
}
